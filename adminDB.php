<?php
class adminDB {
  private $mysqli;

  function __construct() {
    date_default_timezone_set('UTC');
    require_once dirname($_SERVER['DOCUMENT_ROOT']) . '/include/db/DbConnect.php';
    // opening db connection
    $db = new DbConnect();
    //$this->conn = $db->connect();
    $this->mysqli = $db->connect();
  }

  public function getApiKeyByUserName($user_name) {
    $db = new DbConnect();
    $mysqli = $db->connectUserDB();
    $stmt = $mysqli->prepare("SELECT api_key FROM user_table WHERE username = ?");
    $stmt->bind_param("s", $user_name);
    if ($stmt->execute()) {
      // $api_key = $stmt->get_result()->fetch_assoc();
      // TODO
      $stmt->bind_result($api_key);
              $stmt->fetch();
      $stmt->close();
      return $api_key;
    } else {
      return NULL;
    }
  }
}
?>