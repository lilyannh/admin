<?php
require_once("../local_config.php");
sessionsClass::site_protection(true,true,true,false,true);
require_once $includePath . "Config.php";
require_once $includePath . "curlRequest.php";

header('Content-Type: application/json');

$method = urlencode($_GET['method'] );    //Need to url encode
$devid = "";
$server = urlencode($_GET['server'] ); 

$response =  get($server, "", $devid);
echo $response;


 function get($server, $method, $devid) {
    $ch = curl_init();
    $headers = array( 
      "Authorization: Bearer " . SPS_TOKEN
    ); 

    curl_setopt($ch, CURLOPT_URL, urldecode("https://" . $server  . ".spruceirrigation.com:443/v1/devices/". $method));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  //Allow external api to send response
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);

    /* Get Response */
    $response = curl_exec($ch);
    $err = curl_error($ch);
    curl_close($ch);

    if (!empty($err)) return $err;
    if (!empty($response)) {
      return $response;
    }
  }

?>