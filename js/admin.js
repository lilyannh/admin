/*
 $.get("get-wifi.php", {//prepare and execute post
    method: '',
    server: 'sps-01',
  }).done(function(data) {
    greppedObj = jQuery.grep(data, function(element, index){
      return element.connected ===  true; // retain appropriate elements
    });
    $('#sps-01 pre').html(JSON.stringify(greppedObj, null, 2));
    $('#sps-01 span').html(greppedObj.length + ' devices connected.');
    console.log(greppedObj);
  });
*/
   $.get("get-wifi.php", {//prepare and execute post
    method: '',
    server: 'sps-02',
  }).done(function(data) {
    greppedObj = jQuery.grep(data, function(element, index){
      return element.connected ===  true; // retain appropriate elements
    });
    $('#sps-02 pre').html(JSON.stringify(greppedObj, null, 2));
    $('#sps-02 span').html(greppedObj.length + ' devices connected.');
    console.log(greppedObj);
  });

$('#override-button').click( function() {
  overrideAPI($('#username').val());
});

function overrideAPI(username) {
  $.get("get-db.php", {//prepare and execute post
    method: 'overrideAPI',
    var1: username
  }).done(function(data) {
    if (data.apikey !== ''){
      sessionStorage.setItem('apikey', data.apikey);
      toastr.success('Set data for user ' + username + '! Downloading settings...');
      $.post("../js/setVar.php", {//prepare and execute post
        name: 'apikey',
        value: data.apikey
      })
      .success(function(response){
        
        console.log(response)
        $.get("../js/getVar.php", {//prepare and execute post
        })
        .success(function(response){
          console.log(response)
        });
      });
      getSettings(true).done(function(data){
        window.location = '../';
      });
    }
    else {
      toastr.error('Could not match user for apikey :(');
    }
    
  });
}
